#!/bin/sh
feh --bg-scale '/home/jelias/.i3/background0.jpg' '/home/jelias/.i3/background1.jpg' &
compton --config /home/jelias/.config/compton.conf &
unclutter &
nm-applet &
pasystray &
redshift-gtk &
owncloud &
/usr/lib/notification-daemon-1.0/notification-daemon &
#./.conkyconfig/conky_start &
