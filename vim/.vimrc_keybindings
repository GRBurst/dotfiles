let mapleader="\<space>"
let g:mapleader="\<space>"
" Toggles are mapped to <leader>t<something>



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NEO Keyboard Layout (http://neo-layout.org/)

" In NEO we can remap hjkl, because there are arrow keys in the middle of the
" keyboard already.
" Additionally we have: üöäß

" h: Highlight current word, selection, autohighlight
nnoremap <silent> <expr> h HighlightCurrentWord()
vnoremap <silent> h :<C-U>call HighlightSelection()<CR>:set hlsearch<CR>
" toggle automatic highlighting on/off.
nnoremap <Leader>th :if AutoHighlightToggle()<Bar>set hls<Bar>endif<CR>

" j: jump to mark
nnoremap j `
vnoremap j `
" jump to tag (for example in help files)
nnoremap <leader>j <C-]>

" k: (notehing yet)
nnoremap k <Nop>
vnoremap k <Nop>

" l/L: next/prev buffer
nnoremap <silent> l :bnext<CR>
vnoremap <silent> l :bnext<CR>
nnoremap <silent> L :bprev<CR>
vnoremap <silent> L :bprev<CR>

" efficient one-button save/close bindings
nnoremap ö :update<CR>
vnoremap ö <esc>:update<CR>gv
nnoremap Ö :SudoWrite<CR>
vnoremap Ö <esc>:SudoWrite<CR>gv
nnoremap <Leader>ö :update<CR>:call SyntasticAndStatusUpdate()<CR>
nnoremap ä :q<CR>
vnoremap ä <esc>:q<CR>
nnoremap Ä :q!<CR>
vnoremap Ä <esc>:q!<CR>
nnoremap ü :bd<CR>
vnoremap ü <esc>:bd<CR>
nnoremap <Leader>ü :BufOnly<CR>
vnoremap <Leader>ü <esc>:BufOnly<CR>gv

" replay q/f macro
" ß is on the right side next to q
" Q was ex mode
nnoremap ß @q
nnoremap Q @f

" Smart way to move between windows, adjusted for NEO
" in insert mode
imap ∫ <C-o><C-W>h
imap ∀ <C-o><C-W>j
imap Λ <C-o><C-W>k
imap ∃ <C-o><C-W>l
" in other modes
map ∫ <C-W>h
map ∀ <C-W>j
map Λ <C-W>k
map ∃ <C-W>l

" Smart way to move between tabs, adjusted for NEO
" in insert mode
" imap √ <C-o>:tabprev<cr>
" imap ℂ <C-o>:tabnext<cr>
" in other modes
" map √ :tabprev<cr>
" map ℂ :tabnext<cr>




""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Fix some vim annoyances

" TODO: imap <C-r>+ <C-o>"+p

" make Y behave like D and C, yanking till end of line
map Y y$

" paste over rest of line
nnoremap <Leader>p v$<Left>p
" TODO: paste over text objects

" don't overwrite register when pasting over selection
vnoremap p pgvy

" don't lose selection when indenting
vnoremap < <gv
vnoremap > >gv
vnoremap = =gv

" search backwards with backslash
nmap \ ?
vmap \ ?

" delete/edit whole word backwards
nnoremap db xdb
nnoremap cb xcb

" smart home
noremap <expr> <silent> <Home> col('.') == match(getline('.'),'\S')+1 ? '0' : '^'
imap <silent> <Home> <C-O><Home>

" exit insert mode when navigating
" inoremap  <Up>     <Esc><Up>
" inoremap  <Down>   <Esc><Down>
" inoremap  <Left>   <Esc>
" inoremap  <Right>  <Esc>2<Right>
imap  <Home>   <Esc><Home>
imap  <End>    <Esc><End>




""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" custom bindings

" fast way to edit ~/.vimrc*, reload, install plugins
nnoremap <Leader>vv :e ~/.vimrc<CR>
nnoremap <Leader>vp :e ~/.vimrc_plugins<CR>
nnoremap <Leader>vpi :source ~/.vimrc_plugins<CR>:PlugInstall<CR>:source ~/.vimrc_plugins<CR>
nnoremap <Leader>vk :e ~/.vimrc_keybindings<CR>
nnoremap <Leader>vkr :source ~/.vimrc_keybindings<CR>
nnoremap <Leader>vs :e ~/.vimrc_statusline<CR>
nnoremap <Leader>vc :e ~/.vimrc_custom<CR>
nnoremap <Leader>vr :source ~/.vimrc<CR>
nnoremap <Leader>vj :Johannes<CR>

" clear search highlighting
nnoremap <silent> <Leader><Leader> :nohlsearch<CR>
nnoremap <silent> <Leader>/ :nohlsearch<CR>

" TODO: highlight all matches before starting multiple-cursors
" nmap <C-n> :call HighlightCurrentWord()<CR>:!redraw<CR>:call multiple_cursors#new("n",1)<CR>

" location navigation
nnoremap <leader>n :lnext<CR>
nnoremap <leader>N :lprev<CR>

" toggle local spell checking
nnoremap <leader>ts :setlocal spell! spell?<CR>
" TODO: insert best suggestion for last misspelled word
inoremap <c-s> <c-g>u<Esc>[s1z=`]a<c-g>u
nnoremap <c-s> 1z=

" toggle chars at end of line
nmap <silent> <Leader>; <Plug>ToggleEndChar;
nmap <silent> <Leader>, <Plug>ToggleEndChar,






""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins

" Fuzzy file finding
" nmap <silent> <leader>o :FZFTags<CR>
" nmap <silent> <leader>i :FZFTagFile<CR>
nmap <silent> <leader>e :Rooter<CR>:FZFE<CR>:silent! lcd %:p:h<CR>
" nmap <silent> <leader>a :FZFBuffers<CR>
" nmap <silent> <leader>E :FZFE<CR>
" nmap <silent> <leader>; :FZFMru<CR>
" nmap <silent> <Leader>C :FZFColorscheme<CR>
" nmap <silent> <Leader>l :FZFLines<CR>
nmap <leader>a :RFZFAg<space>

" Git
nmap <leader>gn <Plug>GitGutterNextHunk
nmap <leader>gN <Plug>GitGutterPrevHunk
nmap <leader>ga <Plug>GitGutterStageHunk
nmap <leader>gu <Plug>GitGutterStageHunk
nmap <leader>gr <Plug>GitGutterRevertHunk
nmap <leader>gs :silent !tig status<CR>:GitGutterAll<CR>:redraw!<CR>
nmap <silent> <leader>gd :Rooter<CR>

" Autoformat
nmap <leader>f :Autoformat<CR>

" toggle distraction free writing
nnoremap <silent> <leader>tg :Goyo<CR>

" smooth scrolling (Plugin vim-smooth-scroll)
noremap <silent> <c-u> :call smooth_scroll#up(&scroll, 0, 2)<CR>
noremap <silent> <c-d> :call smooth_scroll#down(&scroll, 0, 2)<CR>
noremap <silent> <c-b> :call smooth_scroll#up(&scroll*2, 0, 4)<CR>
noremap <silent> <c-f> :call smooth_scroll#down(&scroll*2, 0, 4)<CR>
noremap <silent> <PageUp> :call smooth_scroll#up(&scroll*2, 0, 4)<CR>
noremap <silent> <PageDown> :call smooth_scroll#down(&scroll*2, 0, 4)<CR>

" vim-schlepp visual movement
vmap <S-up>    <Plug>SchleppUp
vmap <S-down>  <Plug>SchleppDown
vmap <S-left>  <Plug>SchleppLeft
vmap <S-right> <Plug>SchleppRight
nmap <S-up>    <S-v><Plug>SchleppUp
nmap <S-down>  <S-v><Plug>SchleppDown
nmap <S-left>  <S-v><Plug>SchleppLeft
nmap <S-right> <S-v><Plug>SchleppRight

" incsearch plugin
map /  <Plug>(incsearch-forward)
map \  <Plug>(incsearch-backward)
map ?  <Plug>(incsearch-backward)

